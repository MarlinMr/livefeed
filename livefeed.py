import sys, json, praw, time, aiohttp, asyncio
from os.path import expanduser
sys.dont_write_bytecode = True
embedColour = 0x50bdfe
URL = "https://discordapp.com/api"
last_sequence=0

with open(expanduser("~/credentials/discordConfig.json"), 'r',encoding='utf8') as f:
    data = json.load(f)
    TOKEN = data["token"]
    prefix = data["prefix"]
    channelID = data["livefeed"]

async def api_call(path, method="GET", **kwargs):
    """Return the JSON body of a call to Discord REST API."""
    defaults = {
        "headers": {
            "Authorization": f"Bot {TOKEN}",
            "User-Agent": "dBot (https://medium.com/@greut, 0.1)"
        }
    }
    kwargs = dict(defaults, **kwargs)
    async with aiohttp.ClientSession() as session:
        async with session.request(method, path,**kwargs) as response:
            assert 200 == response.status, response.reason
            return await response.json()


async def send_message(recipient_id, content):
    """Send a message with content to the recipient_id."""
    channel = await api_call("/users/@me/channels", "POST",json={"recipient_id": recipient_id})
    return await api_call("/channels/{channel['id']}/messages","POST",json={"content": content})

async def heartbeat(ws, interval):
    """Send every interval ms the heatbeat message."""
    while True:
        await asyncio.sleep(interval / 1000)  # seconds
        await ws.send_json({
            "op": 1,  # Heartbeat
            "d": last_sequence
        })

async def start(url):
    async with aiohttp.ClientSession() as session:
        print(f'{url}?v=6&encoding=json')
        async with session.ws_connect(f"{url}?v=6&encoding=json") as ws:
            async for msg in ws:
                data = json.loads(msg.data)
                if data["op"] == 10:  # Hello
                    print('HELLO : IDENTIFY!')
                    print(data)
                    await ws.send_json({
                        "op": 2,  # Identify
                        "d": {
                            "token": TOKEN,
                            "properties": {},
                            "compress": False,
                            "large_threshold": 250
                        }
                    })
                    print(f'IDENTIFYING : {ws}')
                    asyncio.ensure_future(heartbeat(ws, data['d']['heartbeat_interval']))
                elif data["op"] == 0:  # Dispatch
                    last_sequence=data['s']
                    print(last_sequence)
                elif data["op"] == 11:  # Heartbeat ACK
                    print(data)
                else:
                    pass

async def post_message(url, channelID, message):
    print(f'Connecting to {url} to send {message} to {channelID}')
    channel = await api_call(f'/users/@me/channels', "POST",json={"recipient_id":channelID})
    print(channel)

async def main():
    """Main program."""
    response = await api_call(URL+"/gateway")
    print(f'RESPONSE GIVEN : {response}')
    await asyncio.gather(
        start(response["url"]),
        post_message(response["url"],channelID, 'Test'))

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()


#print(f'Prefix is set to {prefix}')
#print(f'Channel id set as {channelID}')
#with open(expanduser("~/credentials/redditConfig.json"), 'r',encoding='utf8') as f:
#    data = json.load(f)
#    reddit = praw.Reddit(client_id=data["client_id"],
#        client_secret=data["client_secret"],
#        user_agent=data["user_agent"],
#        username=data["username"],
#        password=data["password"])
#print(f'Login with {reddit}')
#sub = data["sub"]
#print(f'Listening to {sub}')
#
#async def main():
#    subreddit = reddit.subreddit(sub)
#    for comment in subreddit.stream.comments(skip_existing=True):
#        print(f'new comment')
#        if(len(comment.body)<2048):
#            try:
#                desc = comment.body
#            except Exception as e:
#                print("Failed to make description.")
#                print(e)
#        else:
#            desc = comment.body[:2043] + "[...]"
#
#        embed = discord.Embed(description=desc, color=embedColour)
#        embed.set_author(name=comment.author,url="https://www.reddit.com/u/"+str(comment.author))
#        embed.add_field(name="Links", value="[permalink](https://old.reddit.com" + comment.permalink + ") [context](https://old.reddit.com" + comment.permalink + "?context=1000)")
#        timeStamp = time.ctime(int(comment.created_utc))+ " UTC"
#        embed.set_footer(text=timeStamp)
#        try:
#            print(f'Sending to Discord')
#        except Exception as e:
#            print(e)
#            bot.run(token, bot=True, reconnect=False)
#
#await main()
